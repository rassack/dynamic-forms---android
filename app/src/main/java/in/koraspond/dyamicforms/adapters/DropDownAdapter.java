package in.koraspond.dyamicforms.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import in.koraspond.dyamicforms.R;

public class DropDownAdapter extends BaseAdapter {

    private Context mContext;
    private String[] values;

    public DropDownAdapter(Context mContext, String[] values) {
        this.mContext = mContext;
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public String getItem(int position) {
        return values[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.dropdown_item, parent, false);
        }
        TextView textView = (TextView) convertView;
        textView.setText(values[position]);
        return convertView;
    }
}
