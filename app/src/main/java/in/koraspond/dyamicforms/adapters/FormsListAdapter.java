package in.koraspond.dyamicforms.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.koraspond.dyamicforms.R;
import in.koraspond.dyamicforms.database.DataSource;
import in.koraspond.dyamicforms.database.tables.Forms;

/**
 * Created by Nizam on 3/20/2018.
 */

public class FormsListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Forms> forms;

    public FormsListAdapter(Context mContext, List<Forms> forms) {
        this.mContext = mContext;
        this.forms = forms;
    }

    @Override
    public int getCount() {
        return forms.size();
    }

    @Override
    public Forms getItem(int position) {
        return forms.get(position);
    }

    @Override
    public long getItemId(int position) {
        return forms.get(position).getFormId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.forms_list_item, parent, false);
        }
        TextView formTitle = convertView.findViewById(R.id.formName);
        TextView formDescription = convertView.findViewById(R.id.formDescription);
        TextView formDate = convertView.findViewById(R.id.date);
        TextView count = convertView.findViewById(R.id.count);
        Forms form = forms.get(position);

        formTitle.setText(form.getFormName());
        formDescription.setText(form.getDescription());
        formDate.setText(form.getCreatedOn());
        count.setText(String.valueOf(form.getFormSubmittedCount()+ DataSource.getPendingFormCount(form.getFormId())));

        return convertView;
    }
}
