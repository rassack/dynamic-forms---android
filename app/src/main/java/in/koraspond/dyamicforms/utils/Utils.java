package in.koraspond.dyamicforms.utils;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import in.koraspond.dyamicforms.R;

public class Utils {

    private static String selectedTime, selectedDateTime;

    public interface DateTimeListener {
        public void onDate(String date);

        public void onTime(String time);
    }

    public static void setMandatory(boolean isMandatory, View view) {
        if (isMandatory) {
            if (view instanceof TextView) {
                TextView text = ((TextView) view);
                if (text.getHint() != null && text.getText().toString().isEmpty()) {
                    text.setHint(text.getHint() + " *");
                } else if (text.getText() != null) {
                    text.setText(text.getText() + " *");
                }
            }
        }
    }

    public static void hideKeyboard(Activity th) {
        View view = th.getCurrentFocus();
        if (view != null) {
            view.clearFocus();
            InputMethodManager inputManager = (InputMethodManager) th.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public static void showDatePicker(Context context, final TextView textView, long minDate, long maxDate) {
        showDatePicker(context, minDate, maxDate, textView);
    }

    public static void showDatePicker(Context context, long minDate, long maxDate, final TextView... textViews) {
        Calendar now = Calendar.getInstance();
        if (textViews.length > 0)
            if (textViews[0] != null && !textViews[0].getText().toString().isEmpty()) {
                try {
                    String FORMAT = "yyyy-MM-dd";
                    Date date = new SimpleDateFormat(FORMAT).parse(textViews[0].getText().toString());
                    if (date != null) {
                        now.setTimeInMillis(date.getTime());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        DatePickerDialog mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                for (TextView textView : textViews)
                    textView.setText(selectedyear + "-" + ((selectedmonth + 1) < 10 ? "0" + (selectedmonth + 1) : (selectedmonth + 1)) + "-" + (selectedday < 10 ? "0" + selectedday : selectedday));
            }
        }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        if (minDate != 0)
            mDatePicker.getDatePicker().setMinDate(minDate);
        if (maxDate != 0)
            mDatePicker.getDatePicker().setMaxDate(maxDate);
        mDatePicker.setTitle(context.getString(R.string.select_date));
        mDatePicker.show();
    }

    public static void showDateTimePicker(final Context context, final TextView textView, final TextView subTextView, long minDate, long maxDate, final boolean isFuture) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog mDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                final String selectedDate = selectedyear + "-" + ((selectedmonth + 1) < 10 ? "0" + (selectedmonth + 1) : (selectedmonth + 1)) + "-" + (selectedday < 10 ? "0" + selectedday : selectedday);
                showTimePickerDateTime(context, textView, subTextView, selectedDate, isFuture);
            }
        }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH));
        if (minDate != 0)
            mDatePicker.getDatePicker().setMinDate(minDate);
        if (maxDate != 0)
            mDatePicker.getDatePicker().setMaxDate(maxDate);
        mDatePicker.setTitle(context.getString(R.string.select_date));
        mDatePicker.show();
    }

    public static void showTimePickerDateTime(final Context context, final TextView textView, final TextView subTextView, final String selectedDate, final boolean isFuture) {
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (!selectedDate.isEmpty()) {
                    if ((isFuture && DateTimeInLong(selectedDate + " " + timeIn12Hour(selectedHour, selectedMinute)) < mcurrentTime.getTimeInMillis())
                            || (!isFuture && DateTimeInLong(selectedDate + " " + timeIn12Hour(selectedHour, selectedMinute)) > mcurrentTime.getTimeInMillis())) {
                        Toast.makeText(context, "Please select a valid time", Toast.LENGTH_LONG).show();
                        showTimePickerDateTime(context, textView, subTextView, selectedDate, isFuture);
                    } else {
                        textView.setText(selectedDate + " " + timeIn12Hour(selectedHour, selectedMinute));
                        selectedDateTime = selectedDate + " " + (selectedHour < 10 ? "0" + selectedHour : selectedHour) + ":" +
                                (selectedMinute < 10 ? "0" + selectedMinute : selectedMinute) + ":00";
                        if (subTextView != null)
                            subTextView.setText(selectedDateTime);
                    }
                } else {
                    Toast.makeText(context, "Please select a date first", Toast.LENGTH_LONG).show();
                }
            }
        }, hour, minute, false);//No 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public static void showTimePicker(final Context context, final TextView textView, final TextView subTextView, final String selectedDate, final boolean isFuture, final DateTimeListener listener) {
        final Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                if (selectedDate != null) {
                    if (!selectedDate.isEmpty()) {
                        if ((isFuture && DateTimeInLong(selectedDate + " " + timeIn12Hour(selectedHour, selectedMinute)) < mcurrentTime.getTimeInMillis())
                                || (!isFuture && DateTimeInLong(selectedDate + " " + timeIn12Hour(selectedHour, selectedMinute)) > mcurrentTime.getTimeInMillis())) {
                            Toast.makeText(context, "Please select a valid time", Toast.LENGTH_LONG).show();
                            showTimePicker(context, textView, subTextView, selectedDate, isFuture, listener);
                        } else {
                            textView.setText(timeIn12Hour(selectedHour, selectedMinute));

                            selectedTime = (selectedHour < 10 ? "0" + selectedHour : selectedHour) + ":" +
                                    (selectedMinute < 10 ? "0" + selectedMinute : selectedMinute) + ":00";
                            if (subTextView != null)
                                subTextView.setText(selectedTime);
                            listener.onTime(selectedTime);
                        }
                    } else {
                        Toast.makeText(context, "Please select a date first", Toast.LENGTH_LONG).show();
                    }
                } else {
                    textView.setText(timeIn12Hour(selectedHour, selectedMinute));
                    selectedTime = (selectedHour < 10 ? "0" + selectedHour : selectedHour) + ":" +
                            (selectedMinute < 10 ? "0" + selectedMinute : selectedMinute) + ":00";
                    if (subTextView != null)
                        subTextView.setText(selectedTime);
                }
            }
        }, hour, minute, false);//No 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public static long DateTimeInLong(String selectedDate) {
        try {
            Calendar calender = Calendar.getInstance();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
            Date date = format.parse(selectedDate);
            /*calender.set(Calendar.MONTH, date.getMonth() - 1);
            calender.set(Calendar.DAY_OF_MONTH, date.getDay());
            calender.set(Calendar.YEAR, date.getYear());*/
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static long getDateInLong(String selectedDate) {
        try {
            Calendar calender = Calendar.getInstance();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = format.parse(selectedDate);
            /*calender.set(Calendar.MONTH, date.getMonth() - 1);
            calender.set(Calendar.DAY_OF_MONTH, date.getDay());
            calender.set(Calendar.YEAR, date.getYear());*/
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String timeIn12Hour(int selectedHour, int selectedMinute) {
        String date = "";
        try {
            DateFormat hour24Format = new SimpleDateFormat("HH:mm");
            DateFormat hour12Format = new SimpleDateFormat("hh:mm aa");

            date = hour12Format.format(hour24Format.parse(selectedHour + ":" + selectedMinute));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static boolean isValidEmail(String email) {
        Pattern EMAIL_ADDRESS = Pattern.compile("^((\"[\\w-\\s]+\")|([\\w-]+(?:\\.[\\w-]+)*)|(\"[\\w-\\s]+\")([\\w-]+(?:\\.[\\w-]+)*))(@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$)|(@\\[?((25[0-5]\\.|2[0-4][0-9]\\.|1[0-9]{2}\\.|[0-9]{1,2}\\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\]?$)");
        return email != null && !email.isEmpty() && EMAIL_ADDRESS.matcher(email).matches();
    }


    public static boolean isValidPhone(String phone) {
        Pattern PHONE = Pattern.compile("((?:\\+|00)[17](?: |\\-)?|(?:\\+|00)[0-9]\\d{0,2}(?: |\\-)?|(?:\\+|00)1\\-\\d{3}(?: |\\-)?)?(0\\d|\\([0-9]{3}\\)|[0-9]{0,3})(?:((?: |\\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\\-)[0-9]{3}(?: |\\-)[0-9]{4})|([0-9]{7}))");
        return phone != null && !phone.isEmpty() && PHONE.matcher(phone).matches();
    }


    public static boolean isValidUrl(String phone) {
        return phone != null && !phone.isEmpty() && Patterns.WEB_URL.matcher(phone).matches();
    }
}
