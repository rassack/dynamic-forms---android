package in.koraspond.dyamicforms.database.tables;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.List;

import in.koraspond.dyamicforms.database.DynamicFormDB;

/**
 * Created by Nizam on 3/20/2018.
 */

@Table(database = DynamicFormDB.class)
public class Forms extends BaseModel {

    @PrimaryKey
    @SerializedName("form_id")
    private int formId;

    @Column
    @SerializedName("form_name")
    private String formName;

    @Column
    @SerializedName("description")
    private String description;

    @Column
    @SerializedName("created_on")
    private String createdOn;

    @Column
    private int formSubmittedCount;

    private List<FormElements> formElements;

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @OneToMany(methods = {OneToMany.Method.ALL}, variableName = "formElements")
    // this should be equal to the field name, that contains children
    public List<FormElements> getFormElements() {
        if (formElements == null || formElements.isEmpty()) {
            formElements = SQLite.select()
                    .from(FormElements.class)
                    .where(FormElements_Table.formId.eq(formId))
                    .queryList();
        }
        return formElements;
    }

    public void setFormElements(List<FormElements> formElements) {
        this.formElements = formElements;
    }

    public int getFormSubmittedCount() {
        return formSubmittedCount;
    }

    public void setFormSubmittedCount(int formSubmittedCount) {
        this.formSubmittedCount = formSubmittedCount;
    }
}
