package in.koraspond.dyamicforms.database;

import android.text.Editable;

import com.google.gson.Gson;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.Date;
import java.util.List;
import java.util.Map;

import in.koraspond.dyamicforms.database.tables.FormData;
import in.koraspond.dyamicforms.database.tables.FormData_Table;
import in.koraspond.dyamicforms.database.tables.Forms;
import in.koraspond.dyamicforms.database.tables.Forms_Table;
import in.koraspond.dyamicforms.database.tables.User;
import in.koraspond.dyamicforms.database.tables.User_Table;

/**
 * Created by Nizam on 3/1/2018.
 */

public class DataSource {
    public static long getUserId() {
        User user = SQLite.select().from(User.class)
                .where(User_Table.loggedIn.eq(true))
                .querySingle();
        return user != null ? user.getUserId() : 0;
    }

    public static List<Forms> getAllForms() {
        return SQLite.select()
                .from(Forms.class)
                .queryList();
    }

    public static void remember(String userName, String password) {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.tableId.eq((long) 1))
                .querySingle();
        if (user != null) {
            user.setUserName(userName);
            user.setPassword(password);
            user.setRemember(true);
            user.update();
        } else {
            User user1 = new User();
            user1.setTableId(1);
            user1.setUserName(userName);
            user1.setPassword(password);
            user1.setRemember(true);
            user1.save();
        }
    }

    public static boolean isUserSaved() {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.remember.eq(true))
                .querySingle();
        return user != null;
    }

    public static String getSavedUsername() {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.remember.eq(true))
                .querySingle();
        return user != null ? user.getUserName() : "";
    }

    public static String getSavedPassword() {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.remember.eq(true))
                .querySingle();
        return user != null ? user.getPassword() : "";
    }

    public static boolean isLoggedIn() {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.loggedIn.eq(true))
                .querySingle();
        return user != null && user.isLoggedIn();
    }

    public static boolean isFormsAdded() {
        return SQLite.select()
                .from(Forms.class)
                .count() > 0;
    }

    public static Forms getFormWithId(int formId) {
        return SQLite.select()
                .from(Forms.class)
                .where(Forms_Table.formId.eq(formId))
                .querySingle();
    }

    public static void saveFormToDB(int formId,Map<String, String> params) {
        FormData formData = new FormData();
        formData.setId(new Date().getTime());
        formData.setData(new Gson().toJson(params));
        formData.setFormId(formId);
        formData.save();
    }


    public static boolean isDataSyncPending() {
        long formsCount = SQLite.select()
                .from(FormData.class)
                .count();
        return formsCount > 0;
    }


    public static int getPendingFormCount(int formId) {
        long formsCount = SQLite.selectCountOf()
                .from(FormData.class)
                .where(FormData_Table.formId.eq(formId))
                .count();
        return (int) formsCount;
    }

    public static List<FormData> getPendingFormData() {
        return SQLite.select()
                .from(FormData.class)
                .queryList();
    }

    public static void removeSyncedForm(long id) {
        FormData formData = SQLite.select()
                .from(FormData.class)
                .where(FormData_Table.id.eq(id))
                .querySingle();
        if (formData != null)
            formData.delete();
    }

    public static String getUpdatedDateTime() {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.loggedIn.eq(true))
                .querySingle();
        return user != null ? user.getUpdatedDateTime() : "";
    }

    public static void setUpdatedDateTime(String dateTime) {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.tableId.eq((long) 1))
                .querySingle();
        if (user != null) {
            user.setUpdatedDateTime(dateTime);
            user.update();
        } else {
            User user1 = new User();
            user1.setTableId(1);
            user1.setUpdatedDateTime(dateTime);
            user1.save();
        }
    }

    public static void logout() {
        User user = SQLite.select()
                .from(User.class)
                .where(User_Table.tableId.eq((long) 1))
                .querySingle();
        if (user != null) {
            user.setLoggedIn(false);
            user.update();
        } else {
            User user1 = new User();
            user1.setLoggedIn(false);
            user1.save();
        }
    }
}
