package in.koraspond.dyamicforms.database;

import com.raizlabs.android.dbflow.annotation.ConflictAction;
import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by Nizam on 3/2/2018.
 */
@Database(name = DynamicFormDB.NAME, version = DynamicFormDB.VERSION,
        insertConflict = ConflictAction.IGNORE, updateConflict = ConflictAction.REPLACE)
public class DynamicFormDB {
    public static final String NAME = "DynamicForm";
    public static final int VERSION = 1;
}
