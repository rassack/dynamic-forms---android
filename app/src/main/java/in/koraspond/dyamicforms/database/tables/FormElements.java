package in.koraspond.dyamicforms.database.tables;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.ForeignKeyReference;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import in.koraspond.dyamicforms.database.DynamicFormDB;

/**
 * Created by Nizam on 3/20/2018.
 */

@Table(database = DynamicFormDB.class)
public class FormElements extends BaseModel {

    public static final int EDIT_TEXT = 1;
    public static final int TEXT_AREA = 2;
    public static final int CHECK_BOX = 3;
    public static final int RADIO_BUTTON = 4;
    public static final int DROP_DOWN = 5;
    public static final int DATE_PICKER = 6;
    public static final int TIME_PICKER = 7;
    public static final int DATE_TIME_PICKER = 8;
    public static final int FILE_UPLOAD = 12;
    public static final int IMAGE = 13;
    public static final int SEPARATOR_LINE = 14;
    public static final int RATING = 15;
    public static final int SIGNATURE = 16;

    @PrimaryKey
    @SerializedName("element_id")
    private int elementId;

    /*@Nullable
    @ForeignKey(tableClass = Forms.class,
            references = @ForeignKeyReference(columnName = "formId", foreignKeyColumnName = "formId"))*/
    @Column
    private int formId;

    @Column
    @SerializedName("field_name")
    private String fieldName;

    @Column
    @SerializedName("variable_name")
    private String variableName;

    @Column
    @SerializedName("field_type")
    private int fieldType;

    @Column
    @SerializedName("status")
    private int status;

    @Column
    @SerializedName("value")
    private String value;

    @Column
    @SerializedName("date_type")
    private int dateType;

    @Column
    @SerializedName("min_char_length")
    private int minLength;

    @Column
    @SerializedName("max_char_length")
    private int maxLength;

    @Column
    @SerializedName("inputType")
    private int inputType;

    @Column
    @SerializedName("line")
    private int line;

    public int getElementId() {
        return elementId;
    }

    public void setElementId(int elementId) {
        this.elementId = elementId;
    }

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public int getFieldType() {
        return fieldType;
    }

    public void setFieldType(int fieldType) {
        this.fieldType = fieldType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getDateType() {
        return dateType;
    }

    public void setDateType(int dateType) {
        this.dateType = dateType;
    }

    public int getMinLength() {
        return minLength;
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public int getInputType() {
        return inputType;
    }

    public void setInputType(int inputType) {
        this.inputType = inputType;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }
}
