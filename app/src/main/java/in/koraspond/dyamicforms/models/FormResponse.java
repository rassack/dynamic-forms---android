package in.koraspond.dyamicforms.models;

import java.util.List;

import in.koraspond.dyamicforms.database.tables.Forms;

/**
 * Created by Nizam on 3/20/2018.
 */

public class FormResponse {

    private int status;

    private int totalCount;

    private String updatedDatetime;

    private List<Forms> fetchForms;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getUpdatedDatetime() {
        return updatedDatetime;
    }

    public void setUpdatedDatetime(String updatedDatetime) {
        this.updatedDatetime = updatedDatetime;
    }

    public List<Forms> getFetchForms() {
        return fetchForms;
    }

    public void setFetchForms(List<Forms> fetchForms) {
        this.fetchForms = fetchForms;
    }
}
