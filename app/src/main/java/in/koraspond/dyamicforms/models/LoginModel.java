package in.koraspond.dyamicforms.models;

import com.google.gson.annotations.SerializedName;

import in.koraspond.dyamicforms.database.tables.User;

/**
 * Created by Nizam on 3/4/2018.
 */

public class LoginModel {

    private int status;

    @SerializedName("sign_in")
    private User user;

    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
