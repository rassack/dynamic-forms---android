package in.koraspond.dyamicforms.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.koraspond.dyamicforms.DynamicForm;
import in.koraspond.dyamicforms.R;
import in.koraspond.dyamicforms.database.DataSource;
import in.koraspond.dyamicforms.database.DynamicFormDB;
import in.koraspond.dyamicforms.models.FormResponse;
import in.koraspond.dyamicforms.network.NetworkRequest;
import in.koraspond.dyamicforms.utils.Constants;

public class DownloadActivity extends AppCompatActivity {

    @BindView(R.id.loader_image)
    ImageView loaderImage;
    int pageNumber = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        ButterKnife.bind(this);

        Glide.with(this)
                .load(R.drawable.download_gif)
                .into(loaderImage);

        getForms();
    }

    private void getForms() {
        String url = Constants.BaseUrl + "app/fetch-user-form-sync";
        NetworkRequest<FormResponse> formsRequest = new NetworkRequest<FormResponse>(Request.Method.POST, url, FormResponse.class,
                null, null,
                new Response.Listener<FormResponse>() {
                    @Override
                    public void onResponse(final FormResponse response) {
                        DatabaseDefinition database = FlowManager.getDatabase(DynamicFormDB.class);
                        Transaction transaction = database.beginTransactionAsync(new ITransaction() {
                            @Override
                            public void execute(DatabaseWrapper databaseWrapper) {
                                for (int i = 0; i < response.getFetchForms().size(); i++) {
                                    response.getFetchForms().get(i).save();
                                    for (int j = 0; j < response.getFetchForms().get(i).getFormElements().size(); j++) {
                                        response.getFetchForms().get(i).getFormElements().get(j).setFormId(response.getFetchForms().get(i).getFormId());
                                        response.getFetchForms().get(i).getFormElements().get(j).save();
                                    }
                                }
                            }
                        }).success(new Transaction.Success() {
                            @Override
                            public void onSuccess(@NonNull Transaction transaction) {
                                if (response.getTotalCount() <= pageNumber * 25) {
                                    DataSource.setUpdatedDateTime(response.getUpdatedDatetime());
                                    startActivity(new Intent(DownloadActivity.this, HomeScreen.class));
                                    finish();
                                } else {
                                    pageNumber++;
                                    getForms();
                                }
                            }
                        }).error(new Transaction.Error() {
                            @Override
                            public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {
                                error.printStackTrace();
                            }
                        }).build();

                        transaction.execute();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        finish();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = super.getParams();
                params.put("user_id", String.valueOf(DataSource.getUserId()));
                params.put("page_no", String.valueOf(pageNumber));
                params.put("updated_date_time", String.valueOf(DataSource.getUpdatedDateTime()));
                return params;
            }
        };

        DynamicForm.getInstance().addToRequestQueue(formsRequest);
    }
}
