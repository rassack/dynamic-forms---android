package in.koraspond.dyamicforms.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import in.koraspond.dyamicforms.R;
import in.koraspond.dyamicforms.database.DataSource;

public class SplashScreen extends AppCompatActivity {


    private static final long DELAY = 2 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //Goto Login/Landing page after 2 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Check login status
                if (DataSource.isLoggedIn()) {
                    if (DataSource.isFormsAdded()) {
                        startActivity(new Intent(SplashScreen.this, HomeScreen.class));
                    } else {
                        startActivity(new Intent(SplashScreen.this, DownloadActivity.class));
                    }

                } else {
                    startActivity(new Intent(SplashScreen.this, Login.class));
                }
                finish();
            }
        }, DELAY);
    }
}
