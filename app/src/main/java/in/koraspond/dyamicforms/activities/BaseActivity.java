package in.koraspond.dyamicforms.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.koraspond.dyamicforms.R;
import in.koraspond.dyamicforms.adapters.DropDownAdapter;
import in.koraspond.dyamicforms.database.DataSource;
import in.koraspond.dyamicforms.database.tables.FormElements;
import in.koraspond.dyamicforms.utils.Constants;
import in.koraspond.dyamicforms.utils.ImageCompression;
import in.koraspond.dyamicforms.utils.ImagePicker;
import in.koraspond.dyamicforms.utils.Utils;

public abstract class BaseActivity extends AppCompatActivity {
    LayoutInflater mLayoutInflater;
    int imageLayoutId = -1;
    public Uri imageCaptureUri;
    Bitmap imageBitmap = null;

    public void setCustomFields(LinearLayout formElementsLayout, List<FormElements> formElements) {
        mLayoutInflater = LayoutInflater.from(this);
        if (formElements != null && formElementsLayout != null) {
            for (int i = 0; i < formElements.size(); i++) {
                FormElements formElement = formElements.get(i);
                View view = getCustomView(formElement.getFieldType(), formElement);
                if (view != null) {
                    formElementsLayout.addView(view);
                }
            }
        }
    }

    private View getCustomView(int fieldType, final FormElements formElement) {
        View view = null;
        switch (fieldType) {
            case FormElements.EDIT_TEXT:
                LinearLayout editTextLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.edittext_layout, null);

                TextView hintText = (TextView) editTextLayout.getChildAt(0);
                EditText editText = (EditText) editTextLayout.getChildAt(1);

                hintText.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, hintText);

                if (formElement.getValue() != null && !formElement.getValue().isEmpty()) {
                    editText.setText(formElement.getValue());
                }
                editText.setInputType(formElement.getInputType());
                editText.setSingleLine(true);

                view = editTextLayout;
                break;
            case FormElements.TEXT_AREA:
                LinearLayout textAreaLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.edittext_layout, null);

                TextView hintText1 = (TextView) textAreaLayout.getChildAt(0);
                EditText editText1 = (EditText) textAreaLayout.getChildAt(1);

                hintText1.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, hintText1);

                if (formElement.getValue() != null && !formElement.getValue().isEmpty()) {
                    editText1.setText(formElement.getValue());
                }
                editText1.setInputType(formElement.getInputType() | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
                editText1.setLines(3);

                view = textAreaLayout;
                break;
            case FormElements.CHECK_BOX:
                LinearLayout checkBoxLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.radio_checkbox_layout, null);
                TextView textView = (TextView) checkBoxLayout.getChildAt(0);
                textView.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, textView);
                LinearLayout linearLayout1 = (LinearLayout) checkBoxLayout.getChildAt(1);
                String[] fieldValues = formElement.getValue().split(",");
                for (int j = 0; j < fieldValues.length; j++) {
                    CheckBox checkBox = new CheckBox(this);
                    checkBox.setText(fieldValues[j]);
                    checkBox.setId(j);

                    linearLayout1.addView(checkBox);
                }
                view = checkBoxLayout;
                break;
            case FormElements.RADIO_BUTTON:
                LinearLayout radioLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.radio_checkbox_layout, null);
                TextView textView1 = (TextView) radioLayout.getChildAt(0);
                textView1.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, textView1);
                LinearLayout linearLayout2 = (LinearLayout) radioLayout.getChildAt(1);
                RadioGroup radioGroup = new RadioGroup(this);
                String[] fieldValues1 = formElement.getValue().split(",");
                for (int j = 0; j < fieldValues1.length; j++) {
                    RadioButton radioButton = new RadioButton(this);
                    radioButton.setChecked(j == 0);
                    radioButton.setId(j);
                    radioButton.setText(fieldValues1[j]);
                    radioGroup.addView(radioButton);
                }
                linearLayout2.addView(radioGroup);
                view = radioLayout;
                break;
            case FormElements.DROP_DOWN:
                LinearLayout dropDownLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.drop_down_layout, null);
                TextView dropdownHint = (TextView) dropDownLayout.getChildAt(0);
                dropdownHint.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, dropdownHint);
                String[] dropDOwnValues = formElement.getValue().split(",");
                Spinner dropDown = (Spinner) dropDownLayout.getChildAt(1);
                dropDown.setAdapter(new DropDownAdapter(this, dropDOwnValues));
                view = dropDownLayout;
                break;
            case FormElements.DATE_PICKER:
                LinearLayout datePickerLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.date_picker_layout, null);
                TextView datePickerText = (TextView) datePickerLayout.getChildAt(0);
                TextView datePicker = (TextView) datePickerLayout.getChildAt(1);
                datePickerText.setText(formElement.getFieldName());
                datePicker.setId(formElement.getElementId());
                Utils.setMandatory(formElement.getStatus() == 2, datePickerText);
                datePicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Utils.showDatePicker(BaseActivity.this, (TextView) view, 0, 0);
                    }
                });
                view = datePickerLayout;
                break;
            case FormElements.TIME_PICKER:
                LinearLayout timePickerLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.time_picker_layout, null);
                TextView timePickerHint = (TextView) timePickerLayout.getChildAt(0);
                final TextView timePicker = (TextView) timePickerLayout.getChildAt(1);
                final TextView timePickerSubText = (TextView) timePickerLayout.getChildAt(2);
                timePickerHint.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, timePickerHint);

                timePicker.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.showTimePicker(BaseActivity.this, timePicker, timePickerSubText,
                                null, true, new Utils.DateTimeListener() {
                                    @Override
                                    public void onDate(String date) {

                                    }

                                    @Override
                                    public void onTime(String time) {
                                        timePicker.setText(time);
                                    }
                                });
                    }
                });
                view = timePickerLayout;
                break;
            case FormElements.DATE_TIME_PICKER:
                LinearLayout dateTimeLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.date_time_picker_layout, null);
                TextView dateTimeHint = (TextView) dateTimeLayout.getChildAt(0);
                final TextView dateTimeText = (TextView) dateTimeLayout.getChildAt(1);
                final TextView dateTimeSubText = (TextView) dateTimeLayout.getChildAt(2);
                dateTimeHint.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, dateTimeHint);

                dateTimeText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.showDateTimePicker(BaseActivity.this, dateTimeText,
                                dateTimeSubText, Calendar.getInstance().getTimeInMillis(),
                                0, true);
                    }
                });
                view = dateTimeLayout;
                break;
            case FormElements.FILE_UPLOAD:
                view = new View(BaseActivity.this);
                break;
            case FormElements.IMAGE:
                LinearLayout imageLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.image_layout, null);

                TextView imageHint = (TextView) imageLayout.getChildAt(0);
                ImageView imageView = (ImageView) imageLayout.getChildAt(1);
                imageView.setImageResource(R.mipmap.ic_launcher);
                imageHint.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, imageHint);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageLayoutId = formElement.getElementId();
                        //Todo: Change saving location.
                        imageCaptureUri = ImagePicker.getOutputMediaFileUri(BaseActivity.this, ImagePicker.MEDIA_TYPE_IMAGE);
                        ImagePicker.selectImage(BaseActivity.this, imageCaptureUri);
                    }
                });
                view = imageLayout;
                break;
            case FormElements.SEPARATOR_LINE:
                view = mLayoutInflater.inflate(R.layout.seperator_layout, null);
                break;
            case FormElements.RATING:
                LinearLayout ratingLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.rating_layout, null);

                TextView ratingHint = (TextView) ratingLayout.getChildAt(0);
                LinearLayout ratingBarLayout = (LinearLayout) ratingLayout.getChildAt(1);

                ratingHint.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, ratingHint);

                view = ratingLayout;
                break;
            case FormElements.SIGNATURE:
                LinearLayout signatureLayout = (LinearLayout) mLayoutInflater.inflate(R.layout.signature_layout, null);
                TextView signatureHint = (TextView) signatureLayout.getChildAt(0);
                LinearLayout signaturePadLayout = (LinearLayout) signatureLayout.getChildAt(1);
                final SignaturePad signaturePad = (SignaturePad) signaturePadLayout.getChildAt(0);
                TextView clear = (TextView) signaturePadLayout.getChildAt(1);
                signatureHint.setText(formElement.getFieldName());
                Utils.setMandatory(formElement.getStatus() == 2, signatureHint);

                clear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        signaturePad.clear();
                    }
                });

                view = signatureLayout;
                break;
        }
        if (view != null)
            view.setId(formElement.getElementId());
        return view;
    }

    public boolean isAllFieldsValid(LinearLayout formElementsLayout, List<FormElements> formElements) {
        boolean valid = true;
        if (formElements != null && formElementsLayout != null) {
            for (int i = 0, k = 0; i < formElements.size(); i++, k++) {
                FormElements formElement = formElements.get(i);
                if (!isValid(formElementsLayout.getChildAt(k), formElement)) {
                    //formElementsLayout.getChildAt(k).requestFocus();
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isValid(View layout, FormElements formElement) {
        int fieldType = formElement.getFieldType();
        switch (fieldType) {
            case FormElements.EDIT_TEXT:
                LinearLayout editTextLayout = (LinearLayout) layout;

                EditText editText = (EditText) editTextLayout.getChildAt(1);

                switch (formElement.getInputType()) {
                    case InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS:
                        if ((formElement.getStatus() == 2 && !Utils.isValidEmail(editText.getText().toString())) ||
                                (!editText.getText().toString().isEmpty() && !Utils.isValidEmail(editText.getText().toString()))) {
                            Toast.makeText(this, "Please enter valid " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                            return false;
                        }
                        break;
                    case InputType.TYPE_CLASS_PHONE:
                        if ((formElement.getStatus() == 2 && !Utils.isValidPhone(editText.getText().toString())) ||
                                (!editText.getText().toString().isEmpty() && !Utils.isValidPhone(editText.getText().toString()))) {
                            Toast.makeText(this, "Please enter valid " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                            return false;
                        }
                        break;
                    case InputType.TYPE_TEXT_VARIATION_URI:
                        if ((formElement.getStatus() == 2 && !Utils.isValidUrl(editText.getText().toString())) ||
                                (!editText.getText().toString().isEmpty() && !Utils.isValidUrl(editText.getText().toString()))) {
                            Toast.makeText(this, "Please enter valid " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                            return false;
                        }
                        break;
                }

                if (!(formElement.getStatus() != 2 || !editText.getText().toString().isEmpty())) {
                    Toast.makeText(this, "Please enter " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.TEXT_AREA:
                LinearLayout textAreaLayout = (LinearLayout) layout;

                EditText editText1 = (EditText) textAreaLayout.getChildAt(1);

                if (!(formElement.getStatus() != 2 || !editText1.getText().toString().isEmpty())) {
                    Toast.makeText(this, "Please enter " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.CHECK_BOX:
                LinearLayout checkBoxLayout = (LinearLayout) layout;

                LinearLayout linearLayout1 = (LinearLayout) checkBoxLayout.getChildAt(1);
                String[] fieldValues = formElement.getValue().split(",");
                int flag = 0;
                for (int j = 0; j < fieldValues.length; j++) {
                    CheckBox checkBox = (CheckBox) linearLayout1.getChildAt(j);
                    if (checkBox.isChecked()) {
                        flag++;
                    }
                }
                if (!(formElement.getStatus() != 2 || flag > 0)) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.RADIO_BUTTON:

                break;
            case FormElements.DROP_DOWN:
                LinearLayout dropDownLayout = (LinearLayout) layout;
                String[] dropDOwnValues = formElement.getValue().split(",");
                Spinner dropDown = (Spinner) dropDownLayout.getChildAt(1);
                if (!(formElement.getStatus() != 2 || dropDown.getSelectedItemId() != -1)) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.DATE_PICKER:
                LinearLayout datePickerLayout = (LinearLayout) layout;

                TextView datePicker = (TextView) datePickerLayout.getChildAt(1);
                if (!(formElement.getStatus() != 2 || !datePicker.getText().toString().isEmpty())) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.TIME_PICKER:
                LinearLayout timePickerLayout = (LinearLayout) layout;
                TextView timePicker = (TextView) timePickerLayout.getChildAt(1);

                if (!(formElement.getStatus() != 2 || !timePicker.getText().toString().isEmpty())) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }

                break;
            case FormElements.DATE_TIME_PICKER:
                LinearLayout dateTimeLayout = (LinearLayout) layout;
                TextView dateTimeText = (TextView) dateTimeLayout.getChildAt(1);

                if (!(formElement.getStatus() != 2 || !dateTimeText.getText().toString().isEmpty())) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.FILE_UPLOAD:

                break;
            case FormElements.IMAGE:
                LinearLayout imageLayout = (LinearLayout) layout;
                TextView base64 = (TextView) imageLayout.getChildAt(2);
                if (!(formElement.getStatus() != 2 || !base64.getText().toString().isEmpty())) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.SEPARATOR_LINE:
                break;
            case FormElements.RATING:
                LinearLayout ratingLayout = (LinearLayout) layout;

                LinearLayout ratingBarLayout = (LinearLayout) ratingLayout.getChildAt(1);
                RatingBar ratingBar = (RatingBar) ratingBarLayout.getChildAt(0);
                if (!(formElement.getStatus() != 2 || ratingBar.getRating() != 0)) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
            case FormElements.SIGNATURE:
                LinearLayout signatureLayout = (LinearLayout) layout;
                LinearLayout signaturePadLayout = (LinearLayout) signatureLayout.getChildAt(1);
                SignaturePad signaturePad = (SignaturePad) signaturePadLayout.getChildAt(0);
                if (!(formElement.getStatus() != 2 || !signaturePad.isEmpty())) {
                    Toast.makeText(this, "Please select " + formElement.getFieldName(), Toast.LENGTH_LONG).show();
                    return false;
                }
                break;
        }
        return true;
    }

    public Map<String, String> getFieldValues(LinearLayout formElementsLayout, List<FormElements> formElements) {
        Map<String, String> params = new HashMap<>();
        if (formElements != null && formElementsLayout != null) {
            params.put("form_id", String.valueOf(formElements.get(0).getFormId()));
            params.put("user_id", String.valueOf(DataSource.getUserId()));
            for (int i = 0, k = 0; i < formElements.size(); i++, k++) {
                FormElements formElement = formElements.get(i);
                params.put(formElement.getVariableName(), getFieldValue(formElementsLayout.getChildAt(i), formElement));
            }
        }
        return params;
    }

    private String getFieldValue(View layout, FormElements formElement) {
        int fieldType = formElement.getFieldType();
        switch (fieldType) {
            case FormElements.EDIT_TEXT:
                LinearLayout editTextLayout = (LinearLayout) layout;

                EditText editText = (EditText) editTextLayout.getChildAt(1);
                return editText.getText().toString();
            case FormElements.TEXT_AREA:
                LinearLayout textAreaLayout = (LinearLayout) layout;

                EditText editText1 = (EditText) textAreaLayout.getChildAt(1);
                return editText1.getText().toString();
            case FormElements.CHECK_BOX:
                LinearLayout checkBoxLayout = (LinearLayout) layout;

                LinearLayout linearLayout1 = (LinearLayout) checkBoxLayout.getChildAt(1);
                String[] fieldValues = formElement.getValue().split(",");

                String checkedValues = "";
                for (int j = 0; j < fieldValues.length; j++) {
                    CheckBox checkBox = (CheckBox) linearLayout1.getChildAt(j);
                    if (checkBox.isChecked()) {
                        if (checkedValues.isEmpty())
                            checkedValues += checkBox.getText().toString();
                        else
                            checkedValues += "," + checkBox.getText().toString();
                    }
                }
                return checkedValues;
            case FormElements.RADIO_BUTTON:
                LinearLayout radioLayout = (LinearLayout) layout;
                LinearLayout linearLayout2 = (LinearLayout) radioLayout.getChildAt(1);
                RadioGroup radioGroup = (RadioGroup) linearLayout2.getChildAt(0);
                String[] fieldValues1 = formElement.getValue().split(",");
                for (int j = 0; j < fieldValues1.length; j++) {
                    RadioButton radioButton = (RadioButton) radioGroup.getChildAt(j);
                    if (radioButton.isChecked()) {
                        return radioButton.getText().toString();
                    }
                }
                break;
            case FormElements.DROP_DOWN:
                LinearLayout dropDownLayout = (LinearLayout) layout;
                String[] dropDOwnValues = formElement.getValue().split(",");
                Spinner dropDown = (Spinner) dropDownLayout.getChildAt(1);
                return dropDOwnValues[(int) dropDown.getSelectedItemId()];
            case FormElements.DATE_PICKER:
                LinearLayout datePickerLayout = (LinearLayout) layout;

                TextView datePicker = (TextView) datePickerLayout.getChildAt(1);
                return datePicker.getText().toString();
            case FormElements.TIME_PICKER:
                LinearLayout timePickerLayout = (LinearLayout) layout;
                TextView timePicker = (TextView) timePickerLayout.getChildAt(1);
                TextView timePickerSubText = (TextView) timePickerLayout.getChildAt(2);
                return timePickerSubText.getText().toString();
            case FormElements.DATE_TIME_PICKER:
                LinearLayout dateTimeLayout = (LinearLayout) layout;
                TextView dateTimeText = (TextView) dateTimeLayout.getChildAt(1);
                TextView dateTimeSubTex = (TextView) dateTimeLayout.getChildAt(2);
                return dateTimeSubTex.getText().toString();
            case FormElements.FILE_UPLOAD:

                break;
            case FormElements.IMAGE:
                LinearLayout imageLayout = (LinearLayout) layout;
                TextView base64 = (TextView) imageLayout.getChildAt(2);
                return base64.getText().toString();
            case FormElements.SEPARATOR_LINE:
                break;
            case FormElements.RATING:
                LinearLayout ratingLayout = (LinearLayout) layout;

                LinearLayout ratingBarLayout = (LinearLayout) ratingLayout.getChildAt(1);
                RatingBar ratingBar = (RatingBar) ratingBarLayout.getChildAt(0);
                return String.valueOf(ratingBar.getRating());
            case FormElements.SIGNATURE:
                LinearLayout signatureLayout = (LinearLayout) layout;
                LinearLayout signaturePadLayout = (LinearLayout) signatureLayout.getChildAt(1);
                SignaturePad signaturePad = (SignaturePad) signaturePadLayout.getChildAt(0);
                Bitmap bitmap = signaturePad.getSignatureBitmap();
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                byte[] image = bytes.toByteArray();
                String signatureBase64 = Base64.encodeToString(image, Base64.DEFAULT);
                return signatureBase64;
        }
        return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.SELECT_IMAGE) {
                new ImageProcessing().execute(data);
            } else if (requestCode == Constants.REQUEST_CAMERA) {
                new ImageProcessing().execute(data);
            }
        }
    }

    private void setImage(String filePath) {
        byte[] image = null;
        if (!filePath.isEmpty()) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            try {
                imageBitmap = BitmapFactory.decodeFile(filePath, bmOptions);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();

                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                image = bytes.toByteArray();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (image != null) {
            if (image.length / 1024 < 2 * 1024) {
                if (imageBitmap != null) {
                    if (imageLayoutId != -1 && findViewById(imageLayoutId) instanceof LinearLayout) {
                        LinearLayout imageLayout = findViewById(imageLayoutId);
                        ImageView imageView = (ImageView) imageLayout.getChildAt(1);
                        TextView imageBase64 = (TextView) imageLayout.getChildAt(2);
                        imageView.setImageBitmap(imageBitmap);

                        String signatureBase64 = Base64.encodeToString(image, Base64.DEFAULT);
                        imageBase64.setText(signatureBase64);
                    }
                }
            } else {
                Toast.makeText(BaseActivity.this, "Image is too big. Please select a small image", Toast.LENGTH_LONG).show();
            }
        }
    }

    public class ImageProcessing extends AsyncTask<Intent, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //AppDialog.showLoader(BaseActivity.this);
        }

        @Override
        protected String doInBackground(Intent... params) {
            Intent data = params[0];
            if (data != null) {
                Uri pictureUri = data.getData();
                if (pictureUri == null) {
                    if (data.getExtras() != null) {
                        try {
                            final Bitmap photo = (Bitmap) data.getExtras().get("data");
                            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                            pictureUri = ImagePicker.getImageUri(BaseActivity.this, photo);

                            if (pictureUri == null)
                                pictureUri = imageCaptureUri;

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (imageCaptureUri != null) {
                        String filePath = new ImageCompression(BaseActivity.this).compressImage(String.valueOf(imageCaptureUri), 800f, 612f);
                        return filePath;
                    }
                }

                String filePath = new ImageCompression(BaseActivity.this).compressImage(String.valueOf(pictureUri), 800f, 612f);
                return filePath;
            } else if (imageCaptureUri != null) {

                String filePath = new ImageCompression(BaseActivity.this).compressImage(String.valueOf(imageCaptureUri), 800f, 612f);
                return filePath;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String image) {
            super.onPostExecute(image);

            if (image != null && !image.isEmpty()) {
                setImage(image);
            }
        }
    }
}
