package in.koraspond.dyamicforms.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.koraspond.dyamicforms.DynamicForm;
import in.koraspond.dyamicforms.R;
import in.koraspond.dyamicforms.database.DataSource;
import in.koraspond.dyamicforms.database.DynamicFormDB;
import in.koraspond.dyamicforms.database.tables.FormElements;
import in.koraspond.dyamicforms.database.tables.Forms;
import in.koraspond.dyamicforms.models.StatusModel;
import in.koraspond.dyamicforms.network.NetworkRequest;
import in.koraspond.dyamicforms.network.NetworkUtil;
import in.koraspond.dyamicforms.utils.Constants;
import in.koraspond.dyamicforms.utils.Utils;

public class AddForm extends BaseActivity {

    @BindView(R.id.customFields)
    LinearLayout customFieldsLayout;

    @BindView(R.id.submitButton)
    TextView submitButton;

    List<FormElements> formElements;
    ProgressDialog mProgressDialog;

    int formId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_form);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            formId = getIntent().getIntExtra(Constants.FormsId, 0);
        } else {
            finish();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Forms forms = DataSource.getFormWithId(formId);
        if (forms != null) {
            formElements = forms.getFormElements();
            setCustomFields(customFieldsLayout, formElements);
        }

    }

    @OnClick(R.id.submitButton)
    public void submitForm() {
        if (isAllFieldsValid(customFieldsLayout, formElements)) {
            Utils.hideKeyboard(this);
            addForm();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_add_form, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_submit) {
            submitForm();
        } else if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void addForm() {
        showProgressDialog("Please wait");
        final Map<String, String> params = getFieldValues(customFieldsLayout, formElements);
        if (NetworkUtil.isConnectedToInternet(this)) {
            String url = Constants.BaseUrl + "app/add-form-details";
            NetworkRequest<StatusModel> request = new NetworkRequest<StatusModel>(Request.Method.POST, url, StatusModel.class,
                    null, null,
                    new Response.Listener<StatusModel>() {
                        @Override
                        public void onResponse(StatusModel response) {
                            if (response != null) {
                                if (response.getStatus() == 1) {
                                    finish();
                                }
                                Toast.makeText(AddForm.this, response.getMessage(), Toast.LENGTH_LONG).show();
                            }
                            dismissProgressDialog();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                            dismissProgressDialog();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return params;
                }
            };
            DynamicForm.getInstance().addToRequestQueue(request);
        } else {
            DatabaseDefinition database = FlowManager.getDatabase(DynamicFormDB.class);
            Transaction transaction = database.beginTransactionAsync(new ITransaction() {
                @Override
                public void execute(DatabaseWrapper databaseWrapper) {
                    DataSource.saveFormToDB(formId, params);
                }
            }).error(new Transaction.Error() {
                @Override
                public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {
                    error.printStackTrace();
                    dismissProgressDialog();
                    Toast.makeText(AddForm.this, "Some Error Occurred", Toast.LENGTH_LONG).show();
                }
            }).success(new Transaction.Success() {
                @Override
                public void onSuccess(@NonNull Transaction transaction) {
                    Toast.makeText(AddForm.this, "Added successfully", Toast.LENGTH_LONG).show();
                    dismissProgressDialog();
                    finish();
                }
            }).build();
            transaction.execute();
        }
    }

    private void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }
}
