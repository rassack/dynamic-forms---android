package in.koraspond.dyamicforms.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.koraspond.dyamicforms.DynamicForm;
import in.koraspond.dyamicforms.R;
import in.koraspond.dyamicforms.adapters.FormsListAdapter;
import in.koraspond.dyamicforms.database.DataSource;
import in.koraspond.dyamicforms.database.DynamicFormDB;
import in.koraspond.dyamicforms.database.tables.Forms;
import in.koraspond.dyamicforms.models.FormResponse;
import in.koraspond.dyamicforms.models.StatusModel;
import in.koraspond.dyamicforms.network.NetworkRequest;
import in.koraspond.dyamicforms.network.NetworkUtil;
import in.koraspond.dyamicforms.utils.Constants;

public class HomeScreen extends AppCompatActivity {

    @BindView(R.id.formsList)
    ListView formsList;

    List<Forms> forms;

    FormsListAdapter formsListAdapter;

    Menu myMenu;
    int pageNumber = 1;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        ButterKnife.bind(this);

        getForms();
    }

    private void getForms() {
        DatabaseDefinition database = FlowManager.getDatabase(DynamicFormDB.class);
        Transaction transaction = database.beginTransactionAsync(new ITransaction() {
            @Override
            public void execute(DatabaseWrapper databaseWrapper) {
                forms = DataSource.getAllForms();
            }
        }).success(new Transaction.Success() {
            @Override
            public void onSuccess(@NonNull Transaction transaction) {
                if (forms != null && forms.size() > 0) {
                    formsListAdapter = new FormsListAdapter(HomeScreen.this, forms);
                    formsList.setAdapter(formsListAdapter);
                    formsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Forms forms = formsListAdapter.getItem(position);
                            Intent intent = new Intent(HomeScreen.this, AddForm.class);
                            intent.putExtra(Constants.FormsId, forms.getFormId());
                            startActivity(intent);
                        }
                    });
                }
            }
        }).error(new Transaction.Error() {
            @Override
            public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {

            }
        }).build();

        transaction.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        myMenu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_refresh) {
            getFormsFromServer();
        } else if (item.getItemId() == R.id.action_logout) {
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setCancelable(true)
                    .setTitle("Logout")
                    .setMessage("Do you want to logout?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            logout();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    private void logout() {
        showProgressDialog("Logging out. Please wait.");
        String url = Constants.BaseUrl + "app/user-logout";
        NetworkRequest<StatusModel> logoutRequest = new NetworkRequest<StatusModel>(Request.Method.POST, url, StatusModel.class,
                null, null,
                new Response.Listener<StatusModel>() {
                    @Override
                    public void onResponse(StatusModel response) {
                        if (response != null) {
                            if (response.getStatus() == 1) {
                                DatabaseDefinition database = FlowManager.getDatabase(DynamicFormDB.class);
                                Transaction transaction = database.beginTransactionAsync(new ITransaction() {
                                    @Override
                                    public void execute(DatabaseWrapper databaseWrapper) {
                                        DataSource.logout();
                                    }
                                }).success(new Transaction.Success() {
                                    @Override
                                    public void onSuccess(@NonNull Transaction transaction) {
                                        Intent intent = new Intent(HomeScreen.this, Login.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finish();
                                    }
                                }).error(new Transaction.Error() {
                                    @Override
                                    public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {
                                        error.printStackTrace();
                                    }
                                }).build();

                                transaction.execute();
                            }
                            dismissProgressDialog();
                            Toast.makeText(HomeScreen.this, response.getMessage(), Toast.LENGTH_LONG).show();
                        }
                        dismissProgressDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        dismissProgressDialog();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = super.getParams();
                params.put("sign_in_id", String.valueOf(DataSource.getUserId()));
                return params;
            }
        };

        DynamicForm.getInstance().addToRequestQueue(logoutRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.isConnectedToInternet(this)) {
            getFormsFromServer();
        } else {
            if (formsListAdapter != null)
                formsListAdapter.notifyDataSetChanged();
        }
    }

    private void getFormsFromServer() {
        if (myMenu != null) {
            // Do animation start
            MenuItem item = myMenu.findItem(R.id.action_refresh);
            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ImageView iv = (ImageView) inflater.inflate(R.layout.iv_refresh, null);
            Animation rotation = AnimationUtils.loadAnimation(this, R.anim.refresh_rotate);
            rotation.setRepeatCount(Animation.INFINITE);
            iv.startAnimation(rotation);
            item.setActionView(iv);
        }

        String url = Constants.BaseUrl + "app/fetch-user-form-sync";
        NetworkRequest<FormResponse> formsRequest = new NetworkRequest<FormResponse>(Request.Method.POST, url, FormResponse.class,
                null, null,
                new Response.Listener<FormResponse>() {
                    @Override
                    public void onResponse(final FormResponse response) {
                        DatabaseDefinition database = FlowManager.getDatabase(DynamicFormDB.class);
                        Transaction transaction = database.beginTransactionAsync(new ITransaction() {
                            @Override
                            public void execute(DatabaseWrapper databaseWrapper) {
                                for (int i = 0; i < response.getFetchForms().size(); i++) {
                                    response.getFetchForms().get(i).save();
                                    for (int j = 0; j < response.getFetchForms().get(i).getFormElements().size(); j++) {
                                        response.getFetchForms().get(i).getFormElements().get(j).setFormId(response.getFetchForms().get(i).getFormId());
                                        response.getFetchForms().get(i).getFormElements().get(j).save();
                                    }
                                }
                            }
                        }).success(new Transaction.Success() {
                            @Override
                            public void onSuccess(@NonNull Transaction transaction) {
                                if (response.getTotalCount() <= pageNumber * 25) {
                                    DataSource.setUpdatedDateTime(response.getUpdatedDatetime());
                                    resetUpdating();
                                    getForms();
                                } else {
                                    pageNumber++;
                                    getFormsFromServer();
                                }
                            }
                        }).error(new Transaction.Error() {
                            @Override
                            public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {
                                error.printStackTrace();
                                resetUpdating();
                            }
                        }).build();

                        transaction.execute();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        resetUpdating();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = super.getParams();
                params.put("user_id", String.valueOf(DataSource.getUserId()));
                params.put("page_no", String.valueOf(pageNumber));
                params.put("updated_date_time", String.valueOf(DataSource.getUpdatedDateTime()));
                return params;
            }
        };

        DynamicForm.getInstance().addToRequestQueue(formsRequest);
    }

    public void resetUpdating() {
        if (myMenu != null) {
            // Get our refresh item from the menu
            MenuItem m = myMenu.findItem(R.id.action_refresh);
            if (m.getActionView() != null) {
                // Remove the animation.
                m.getActionView().clearAnimation();
                m.setActionView(null);
            }
        }
    }
}
