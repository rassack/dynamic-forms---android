package in.koraspond.dyamicforms.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.raizlabs.android.dbflow.config.DatabaseDefinition;
import com.raizlabs.android.dbflow.config.FlowManager;
import com.raizlabs.android.dbflow.structure.database.DatabaseWrapper;
import com.raizlabs.android.dbflow.structure.database.transaction.ITransaction;
import com.raizlabs.android.dbflow.structure.database.transaction.Transaction;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.koraspond.dyamicforms.DynamicForm;
import in.koraspond.dyamicforms.R;
import in.koraspond.dyamicforms.database.DataSource;
import in.koraspond.dyamicforms.database.DynamicFormDB;
import in.koraspond.dyamicforms.models.LoginModel;
import in.koraspond.dyamicforms.network.NetworkRequest;
import in.koraspond.dyamicforms.network.NetworkUtil;
import in.koraspond.dyamicforms.utils.Constants;

public class Login extends AppCompatActivity {

    @BindView(R.id.signIn)
    TextView signIn;

    @BindView(R.id.username)
    EditText username;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.rememberMe)
    CheckBox rememberMe;

    Snackbar snackbar;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        rememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !username.getText().toString().isEmpty()
                        && !password.getText().toString().isEmpty()) {
                    DataSource.remember(username.getText().toString(), password.getText().toString());
                }
            }
        });

        if (DataSource.isUserSaved()) {
            username.setText(DataSource.getSavedUsername());
            password.setText(DataSource.getSavedPassword());
            rememberMe.setChecked(true);
        }

    }

    @OnClick(R.id.signIn)
    public void SignIn() {
        if (!username.getText().toString().isEmpty()) {
            if (!password.getText().toString().isEmpty()) {
                if (NetworkUtil.isConnectedToInternet(this)) {
                    showProgressDialog("Please wait");
                    String url = Constants.BaseUrl + "app/user-login";
                    NetworkRequest<LoginModel> loginRequest = new NetworkRequest<LoginModel>(Request.Method.POST, url,
                            LoginModel.class, null, null,
                            new Response.Listener<LoginModel>() {
                                @Override
                                public void onResponse(final LoginModel response) {
                                    if (response.getStatus() == 1) {
                                        DatabaseDefinition database = FlowManager.getDatabase(DynamicFormDB.class);
                                        Transaction transaction = database.beginTransactionAsync(new ITransaction() {
                                            @Override
                                            public void execute(DatabaseWrapper databaseWrapper) {
                                                response.getUser().setLoggedIn(true);
                                                response.getUser().setTableId(1);
                                                response.getUser().save();
                                            }
                                        }).success(new Transaction.Success() {
                                            @Override
                                            public void onSuccess(@NonNull Transaction transaction) {
                                                startActivity(new Intent(Login.this, DownloadActivity.class));
                                                finish();
                                            }
                                        }).error(new Transaction.Error() {
                                            @Override
                                            public void onError(@NonNull Transaction transaction, @NonNull Throwable error) {

                                            }
                                        }).build();

                                        transaction.execute();
                                    }
                                    dismissProgressDialog();
                                    Toast.makeText(Login.this, response.getMessage(), Toast.LENGTH_LONG).show();

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();
                                    dismissProgressDialog();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String, String> params = super.getParams();
                            params.put("user_name", username.getText().toString());
                            params.put("password", password.getText().toString());
                            return params;
                        }
                    };
                    DynamicForm.getInstance().addToRequestQueue(loginRequest);
                } else {
                    snackbar = Snackbar.make(signIn, R.string.enable_internet_and_retry, Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.retry, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    snackbar.dismiss();
                                    SignIn();
                                }
                            });

                    snackbar.show();
                }
            } else {
                snackbar = Snackbar.make(signIn, R.string.enter_userName, Snackbar.LENGTH_SHORT);
                username.requestFocus();
                snackbar.show();
            }
        } else {
            snackbar = Snackbar.make(signIn, R.string.enter_password, Snackbar.LENGTH_SHORT);
            password.requestFocus();
            snackbar.show();
        }
    }

    private void showProgressDialog(String message) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }
        mProgressDialog.setMessage(message);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.show();
    }

    private void dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }
}
