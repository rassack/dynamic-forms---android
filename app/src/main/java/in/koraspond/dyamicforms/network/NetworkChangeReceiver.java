package in.koraspond.dyamicforms.network;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Map;

import in.koraspond.dyamicforms.DynamicForm;
import in.koraspond.dyamicforms.database.DataSource;
import in.koraspond.dyamicforms.database.tables.FormData;
import in.koraspond.dyamicforms.models.StatusModel;
import in.koraspond.dyamicforms.utils.Constants;

/**
 * Created by Nizam on 1/3/2017.
 */

public class NetworkChangeReceiver extends WakefulBroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (DataSource.isDataSyncPending()) {
            if (NetworkUtil.isConnectedToInternet(context)) {
                List<FormData> forms = DataSource.getPendingFormData();
                if (forms!=null && forms.size()>0) {
                    for (final FormData formData:forms) {
                        String url = Constants.BaseUrl + "app/add-form-details";
                        NetworkRequest<StatusModel> request = new NetworkRequest<StatusModel>(Request.Method.POST, url, StatusModel.class,
                                null, null,
                                new Response.Listener<StatusModel>() {
                                    @Override
                                    public void onResponse(StatusModel response) {
                                        if (response != null) {
                                            Log.e("Add Form", response.getMessage());
                                            if (response.getStatus()==1){
                                                DataSource.removeSyncedForm(formData.getId());
                                            }
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.printStackTrace();
                                    }
                                }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError{
                                Map<String,String> params = new Gson().fromJson(formData.getData(),new TypeToken<Map<String,String>>(){}.getType());
                                return params;
                            }
                        };

                        DynamicForm.getInstance().addToRequestQueue(request);
                    }
                }
            }
        }
    }
}
