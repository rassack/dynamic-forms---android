package in.koraspond.dyamicforms.network;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPInputStream;

public class NetworkRequest<T> extends Request<T> {
    private final Gson gson = new Gson();
    private final Class<T> mClass;
    private final Map<String, String> mHeaders;
    private final String mRequestBody;
    private final Response.Listener<T> mListener;
    private final Response.ErrorListener mErrorListener;
    private static final String GZIP_ENCODIING_TYPE = "gzip";


    public NetworkRequest(int method, String url, Class<T> clazz, Map<String, String> headers, String requestBody,
                          Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        if (headers == null)
            headers = new HashMap<>();
        //headers.put("Accept-Encoding", GZIP_ENCODIING_TYPE);
        //headers.put("Content-Encoding", GZIP_ENCODIING_TYPE);
        this.mClass = clazz;
        this.mHeaders = headers;
        this.mRequestBody = requestBody;
        this.mListener = listener;
        this.mErrorListener = errorListener;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return mHeaders != null ? mHeaders : super.getHeaders();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = null;
            String contentType = response.headers.get("Content-Encoding");
            if (contentType != null && contentType.equalsIgnoreCase(GZIP_ENCODIING_TYPE)) {
                GZIPInputStream zis = new GZIPInputStream(new BufferedInputStream(new ByteArrayInputStream(response.data)));
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;
                while ((count = zis.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                }
                byte[] bytes = baos.toByteArray();
                jsonString = new String(bytes);
            } else {
                jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            }
            //String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            if (mClass.equals(String.class)){
                return (Response<T>) Response.success(jsonString,
                        HttpHeaderParser.parseCacheHeaders(response));
            }
            return Response.success(gson.fromJson(jsonString, mClass),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        } catch (IOException e) {
            return Response.error(new ParseError(e));
        }
    }


    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }

    @Override
    public String getBodyContentType() {
        return super.getBodyContentType()/*"application/json; charset=utf-8"*/;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        Calendar calendar = Calendar.getInstance();
        Map<String, String> params = new HashMap<>();
        //params.put("Accept-Encoding", GZIP_ENCODIING_TYPE);
        params.put("user_timezone_app_offset", String.valueOf(calendar.getTimeZone().getRawOffset()/1000));
        return params;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
       /* try {
            return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mRequestBody, "utf-8");
            return null;
        }*/
        return super.getBody();
    }
}